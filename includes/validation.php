<?php
// define variables and set to empty values
        $centerErr = $cityErr = $stateErr = $countryErr = $descriptionErr = $phoneErr = $listingserviceErr = $emailErr = $boolErr = "";
        $center = $city = $state = $country = $description = $phone = $listingservice = $email = $bool = "";

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (empty($_POST["center"])) {
                $centerErr = "center is required";
            } else {
                $center = test_input($_POST["center"]);
                // check if name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/", $center)) {
                    $centerErr = "Only letters and white space allowed";
                }
            }
//$getthevalueofid = $_GET['id'];
            if (empty($_POST["variable"])) {
                $cityErr = "city is required";
            } else {
                $city = test_input($_POST["variable"]);
                var_dump($city);
                // check if name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/", $city)) {
                    $cityErr = "Only letters and white space allowed";
                }
            }

            if (empty($_POST["state"])) {
                $stateErr = "state is required";
            } else {
                $state = test_input($_POST["state"]);
                // check if name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/", $state)) {
                    $stateErr = "Only letters and white space allowed";
                }
            }

            if (empty($_POST["country"])) {
                $countryErr = "country is required";
            } else {
                $country = test_input($_POST["country"]);
                // check if name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/", $country)) {
                    $countryErr = "Only letters and white space allowed";
                }
            }

            if (empty($_POST["description"])) {
                $descriptionErr = "description is required";
            } else {
                $description = test_input($_POST["description"]);
                // check if name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/", $description)) {
                    $descriptionErr = "Only letters and white space allowed";
                }
            }

            if (empty($_POST["phone"])) {
                $phoneErr = "phone is required";
            } else {
                $phone = test_input($_POST["phone"]);
                // check if name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/", $phone)) {
                    $phoneErr = "Only letters and white space allowed";
                }
            }

            if (empty($_POST["listingservice"])) {
                $listingserviceErr = "Listingservice is required";
            } else {
                $name = test_input($_POST["listingservice"]);
                // check if name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/", $name)) {
                    $listingserviceErr = "Only letters and white space allowed";
                }
            }

            if (empty($_POST["email"])) {
                $emailErr = "Email is required";
            } else {
                $email = test_input($_POST["email"]);
                // check if e-mail address is well-formed
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $emailErr = "Invalid email format";
                }
            }

            if (empty($_POST["bool"])) {
                $$boolErr = "Answer is required";
            } else {
                $bool = test_input($_POST["bool"]);
            }
        }

        function test_input($data) {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }

