-- creating database
CREATE DATABASE mycompany;
-- create table
CREATE TABLE IF NOT EXISTS `master` (
    `id` int(30) NOT NULL,
    `center_name` varchar(100) NOT NULL,
    `city` varchar(60) NOT NULL,
    `state` varchar(60) NOT NULL,
    `country` varchar(60) NOT NULL,
    `listingservice` varchar(100) NOT NULL,
    `email` varchar(20) NOT NULL,
    `phone` int(11) NOT NULL,
    `live` tinyint(1) NOT NULL,
    `description` text
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- dumping table master

INSERT INTO `master` (`id`,  `center_name`,  `city` , `state`,  `country`,  `description`,  `phone`,  `listingservice`,  `email`, `live` ) VALUES
(1, 'center_name', 'city', 'state',  'country',  '<p>description</p>',  060444444,  'listingservice',  'email@gmail.com', 1 );
