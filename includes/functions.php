<?php
function listFolder($dir) {
    $dirContent = scandir($dir);
    $dataArray = array();
    foreach ($dirContent as $data) {
        if ($data != '.' && $data != '..') {
            $dataArray[] = $data;
        }
    }
    return $dataArray;
}

