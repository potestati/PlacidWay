<?php require('./includes/db-connect.php'); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
        <meta name="description" content="PlacidWay">
        <meta name="keywords" content="PlacidWay">
        <meta name="author" content="PlacidWay">
        <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
        <link rel="icon" href="img/favicon.png" type="image/x-icon">
        <title>PlacidWay</title>
        <!-- Bootstrap CSS -->
        <link href="css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <!--  Main CSS-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <!-- Responsive CSS -->
        <link rel="stylesheet" type="text/css" href="css/responsive.css">
        <script src="https://cdn.ckeditor.com/ckeditor5/0.11.0/classic/ckeditor.js"></script>
    </head>
    <body>
        <?php
        //include('./includes/validation.php'); 
// define variables and set to empty values
        $centerErr = $cityErr = $stateErr = $countryErr = $descriptionErr = $phoneErr = $listingserviceErr = $emailErr = $boolErr = "";
        $center = $city = $state = $country = $description = $phone = $listingservice = $email = $bool = "";

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if (empty($_POST["center"])) {
                $centerErr = "center is required";
            } else {
                $center = test_input($_POST["center"]);
                // check if name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/", $center)) {
                    $centerErr = "Only letters and white space allowed";
                }
            }
            if (empty($_POST['city'])) {
                $cityErr = "city is required";
            } else {
                $city = test_input($_POST['city']);
                // check if name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/", $city)) {
                    $cityErr = "Only letters and white space allowed";
                }
            }

            if (empty($_POST["state"])) {
                $stateErr = "state is required";
            } else {
                $state = test_input($_POST["state"]);
                // check if name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/", $state)) {
                    $stateErr = "Only letters and white space allowed";
                }
            }

            if (empty($_POST["country"])) {
                $countryErr = "country is required";
            } else {
                $country = test_input($_POST["country"]);
                // check if name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/", $country)) {
                    $countryErr = "Only letters and white space allowed";
                }
            }

            if (empty($_POST["content"])) {
                $descriptionErr = "description is required";
                var_dump($description);
            } else {
                $description = test_input($_POST["content"]);
                // check if name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/", $description)) {
                    $descriptionErr = "Only letters and white space allowed";
                }
                var_dump($description);
            }

            if (empty($_POST["phone"])) {
                $phoneErr = "phone is required";
            } else {
                $phone = test_input($_POST["phone"]);
                // check if name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/", $phone)) {
                    $phoneErr = "Only letters and white space allowed";
                }
            }

            if (empty($_POST["listingservice"])) {
                $listingserviceErr = "Listingservice is required";
            } else {
                $listingservice = test_input($_POST["listingservice"]);
                // check if name only contains letters and whitespace
                if (!preg_match("/^[a-zA-Z ]*$/", $name)) {
                    $listingserviceErr = "Only letters and white space allowed";
                }
            }

            if (empty($_POST["email"])) {
                $emailErr = "Email is required";
            } else {
                $email = test_input($_POST["email"]);
                // check if e-mail address is well-formed
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $emailErr = "Invalid email format";
                }
            }

            if (empty($_POST["bool"])) {
                $$boolErr = "Answer is required";
            } else {
                $bool = test_input($_POST["bool"]);
            }
        }

        function test_input($data) {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }
        ?>

        <div id="cityList">
        </div>
        <div class="container">
            <form id="myForm" class="form-horizontal" role="form" method="post">
                <h2>Master Form</h2>
                <!--center_name -->
                <div class="form-group">
                    <label for="center_name" class="col-sm-3 control-label">Center Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="center" placeholder="Center Name" class="form-control" autofocus>
                    </div>
                </div>
                <!-- city-->
                <div class="form-group">
                    <label for="city" class="col-sm-3 control-label">City</label>
                    <div class="col-sm-9">
                        <select id="city" class="form-control" name="city">
                            <option value=''>Select City</option>
                            <?php include('city.php'); ?>
                        </select>
                    </div>
                </div>
                <!--state -->
                <div class="form-group">
                    <label for="state" class="col-sm-3 control-label">State</label>
                    <div class="col-sm-9">
                        <input type="text" placeholder="State" name="state" class="form-control" autofocus>
                    </div>
                </div>
                <!-- country-->
                <div class="form-group">
                    <label for="country" class="col-sm-3 control-label">Country</label>
                    <div class="col-sm-9">
                        <select id="country" name="country" class="form-control">
                            <option value=''>Select Country</option>
                            <?php include('country.php'); ?>
                        </select>
                    </div>
                </div>
                <!-- description -->
                <div class="form-group">
                    <label for="description" class="col-sm-3 control-label">Description</label>
                    <div class="col-sm-9">
                        <textarea name="content" id="editor" rows="10" cols="80"></textarea>
                        <script>
                            ClassicEditor
                                    .create(document.querySelector('#editor'))
                                    .then(editor => {
                                        console.log(editor);
                                    })
                                    .catch(error => {
                                        console.error(error);
                                    });
                        </script>
                    </div>
                </div>
                <!-- phone -->
                <div class="form-group">
                    <label for="phone" class="col-sm-3 control-label">Phone</label>
                    <div class="col-sm-9">
                        <input type="text" name="phone" placeholder="Phone" class="form-control" autofocus>
                    </div>
                </div>
                <!--listingservice -->
                <div class="form-group">
                    <label for="listingservice" class="col-sm-3 control-label">Listingservice</label>
                    <div class="col-sm-9">
                        <input type="text" name="listingservice" placeholder="Listingservice" class="form-control" autofocus>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-9">
                        <input type="email" id="email" name="email" placeholder="Email" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Yes/No</label>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="radio-inline">
                                    <input type="radio" name="bool" <?php if (isset($bool) && $bool == "yes") echo "checked"; ?> value="yes">Yes
                                    <span class="error">* <?php echo $boolErr; ?></span>
                                </label>
                            </div>
                            <div class="col-sm-4">
                                <label class="radio-inline">
                                    <input type="radio" id="maleRadio" name="bool" <?php if (isset($bool) && $bool == "no") echo "checked"; ?>  value="no">No
                                    <span class="error">* <?php echo $boolErr; ?></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div> <!-- /.form-group -->
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <button id="btn" type="submit" name="submit" class="btn btn-primary btn-block">Submit Data</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="container">
            <?php
            echo "<h2>Your Input:</h2>";
            echo $center;
            echo "<br>";
            echo $city;
            echo "<br>";
            echo $state;
            echo "<br>";
            echo $country;
            echo "<br>";
            echo $description;
            echo "<br>";
            echo $phone;
            echo "<br>";
            echo $listingservice;
            echo "<br>";
            echo $email;
            echo "<br>";
            echo $bool;
            ?>
        </div>
        <!-- Start Script-->
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <!-- Main Script -->
        <script src="js/main.js" type="text/javascript"></script>
        <script type="text/javascript">

                            $('#city').on('change', function () {
                                var e = document.getElementById("city");
                                var data = e.value;
                                console.log(data);
                                $.ajax({
                                    type: "post",
                                    url: "city.php",
                                    data: data,
                                    success: function () {
                                        $('#cityList').html(data);
                                    }
                                });
                            });


        </script>
        <div><>
        <!-- / Script-->
    </body>
</html>
